#!/bin/bash

PS3='Please enter your choice: '
options=(
  "Build Apk"
  "One time generation *.g.dart files"
  "Continuously build generation"
  "Splash Screen"
  "Launcher Icon"
  "Clean"
  "Quit"
)

select opt in "${options[@]}"
do
  case $opt in
    "Build Apk")
      # Release Apk can be found at ~/build/app/outputs/flutter-apk
      flutter build apk --release
      break
      ;;
    "One time generation *.g.dart files")
      echo "${GREEN}More info: https://flutter.dev/docs/development/data-and-backend/json#one-time-code-generation ${reset}"
      flutter clean
      flutter packages pub upgrade
      flutter pub run build_runner build --delete-conflicting-outputs
      break
      ;;
    "Continuously build generation")
      flutter clean
      flutter packages pub upgrade
      flutter pub run build_runner watch --delete-conflicting-outputs
      ;;
    "Splash Screen")
      flutter pub pub run flutter_native_splash:create
      break
      ;;
    "Launcher Icon")
      flutter pub run flutter_launcher_icons:main -f flutter_launcher_icons.yaml
      break;;
    "Clean")
      flutter clean
      break
      ;;
    "Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac
done