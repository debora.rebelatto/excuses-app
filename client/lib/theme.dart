import 'package:flutter/material.dart';

theme(brightness) {
  return ThemeData (
    scaffoldBackgroundColor: Color(0xff2C44C9),
    backgroundColor: Colors.grey[100],
    cardColor: Colors.white,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Color.fromRGBO(64, 75, 96, 1),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: Colors.black)
    ),
    textTheme: TextTheme( headline1: TextStyle(color: Colors.black) ),
    primaryTextTheme: TextTheme(
      headline1: TextStyle( color: Colors.black ),
      subtitle1: TextStyle( color: Colors.black ),
      subtitle2: TextStyle( color: Color(0xff191919) )
    ),
    appBarTheme: AppBarTheme( color: Color.fromRGBO(64, 75, 96, 1) ),
    bottomAppBarColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black87),
    brightness: brightness,
  );
}
