import 'package:flutter/material.dart';

facebookButton(context){
  return Container(
    padding: EdgeInsets.only(top: 20),
    width: MediaQuery.of(context).size.width * 0.9,
    child: FlatButton(
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
      color: Color(0xff7583CA),
      child: ListTile(
        leading: Image.asset('assets/images/Facebook.png', height: 30,),
        title: Text('CONTINUAR COM FACEBOOK', textAlign: TextAlign.center, style: TextStyle(color: Colors.white ),),
      ),
      onPressed: (){},
    )
  );
}