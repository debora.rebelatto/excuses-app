abstract class ValidateFields {
  static String validateIfEmpty(value) => value.isEmpty ? 'Preencher campo' : null;

  static String validateEmail(value) {
    if(value.isEmpty) {
      return 'Preencher campo.';
    } else if(RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"
    ).hasMatch(value) == false) {
      return 'Formato inválido.';
    }
    return null;
  }
}