import 'package:desculpas_chefe/Templates/Components/ValidateFields.dart';
import 'package:desculpas_chefe/Templates/Signup/Components/fieldDecoration.dart';
import 'package:flutter/material.dart';

class WidgetForgotPassWord extends StatefulWidget{
  _StateWidgetForgotPassword createState() => _StateWidgetForgotPassword();
}

class _StateWidgetForgotPassword extends State<WidgetForgotPassWord> {
  TextEditingController _email = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfffafafa),
      appBar: AppBar(
        title: Text('Esqueceu a senha?'),
        backgroundColor: Color(0xff2C44C9)
      ),
      body: Container(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Preencha o email:', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: _email,
                decoration: decoration('E-mail', Icons.person),
                validator: (value) => ValidateFields.validateEmail(value),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
              ),
            ),

          ],
        ),
      ),
    );
  }
}