import 'package:desculpas_chefe/Templates/Components/facebookButton.dart';
import 'package:desculpas_chefe/Templates/Components/googleButton.dart';
import 'package:desculpas_chefe/Templates/Signup/Components/emailFieldSignup.dart';
import 'package:desculpas_chefe/Templates/Signup/Components/passwordFieldSignup.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'Components/careerFieldSignup.dart';
import 'Components/nameFieldSignup.dart';
import 'Components/signupButton.dart';

class WidgetSignup extends StatefulWidget{
  _WidgetSignupState createState() => _WidgetSignupState();
}

class _WidgetSignupState extends State<WidgetSignup> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController(text: "");
  TextEditingController _email = TextEditingController(text: "");
  TextEditingController _password = TextEditingController(text: "");
  TextEditingController _career = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfffafafa),
      body: Stack(
        children: [
          NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[ SliverAppBar(
                iconTheme: IconThemeData( color: Colors.black ),
                title: Text('Cadastre-se', style: TextStyle(color:Colors.black)),
                backgroundColor: Colors.transparent
              )];
            },
            body: Container(
              padding: EdgeInsets.all(20),
              child:
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    googleButton(context),
                    facebookButton(context),

                    SizedBox(height: 20),

                    Text('OU', style: GoogleFonts.abel(color: Color(0xff2C44C9), fontSize: 20)),

                    SizedBox(height: 20),

                    nameFieldSingup(_name),
                    emailFieldSignup(_email),
                    passwordFieldSignup(_password),
                    careerFieldSignup(_career),
                    SizedBox(height: 10),
                    SignupButton(_name.text, _email.text, _password.text, _career.text, _formKey),
                  ]
                )
              )
            )
          ),
        ],
      ),
    );
  }
}