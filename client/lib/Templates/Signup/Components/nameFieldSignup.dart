import 'package:desculpas_chefe/Templates/Components/ValidateFields.dart';
import 'package:flutter/material.dart';

import 'fieldDecoration.dart';

nameFieldSingup(_name) {
  return Builder(builder: (context) => Padding(
    padding: EdgeInsets.only(bottom: 20),
    child: TextFormField(
      controller: _name,
      decoration: decoration('Nome', Icons.person),
      textInputAction: TextInputAction.next,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
      validator: (value) => ValidateFields.validateIfEmpty(value),
    ),
  ));
}