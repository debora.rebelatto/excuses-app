import 'package:flutter/material.dart';

decoration(hintText, icon) {
  return InputDecoration(
    prefixIcon: Icon(icon),
    border: new OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.all(Radius.circular(30.0)),
    ),
    filled: true,
    fillColor: Colors.grey[200],
    hintText: hintText,
    
  );
}