import 'package:flutter/material.dart';

import 'fieldDecoration.dart';

careerFieldSignup(_career) {
  return Builder(
    builder: (context) => Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: TextFormField(
        controller: _career,
        decoration: decoration('Profissão', Icons.wallet_travel_outlined),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
      ),
    )
  );
}