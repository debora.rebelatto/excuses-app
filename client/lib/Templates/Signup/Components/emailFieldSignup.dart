import 'package:desculpas_chefe/Templates/Components/ValidateFields.dart';
import 'package:flutter/material.dart';

import 'fieldDecoration.dart';

emailFieldSignup(_email) {
  return Builder(
    builder: (context) => Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _email,
        decoration: decoration('Email', Icons.mail_outline),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        validator: (value) => ValidateFields.validateEmail(value),
      ),
    )
  );
}