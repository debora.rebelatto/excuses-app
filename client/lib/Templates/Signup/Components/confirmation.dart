import 'dart:convert';

import 'package:desculpas_chefe/Contollers/SharedPrefController.dart';
import 'package:desculpas_chefe/Contollers/UserController.dart';
import 'package:desculpas_chefe/Templates/Homepage/WidgetHomepage.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

confirmation(res, context, _email, _password) async {
  if(res.statusCode == 200) {
    await showDialog(
      barrierDismissible: false,
      context: context,
      child: AlertDialog(
        title: Column(children: [
          Lottie.asset(
            'assets/images/37200-good-coche.json',
            height: 100, repeat: false, animate: true,
          ),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Text('Usuário Cadastrado!')
          )
        ],),
        actions: [
          FlatButton(
            child: Text("Continuar"),
            onPressed: () async {
              var res = await UserController.login(email: _email, password: _password);
              if(res.statusCode == 200) {
                await SharedPrefController.saveUser(res);
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetHomePage()));
              }
            },
          )
        ],
      )
    );
  } else {
    var decode = jsonDecode(res.body);
    showDialog(
      context: context,
      child: AlertDialog(
        title: Text("Tente Novamente"),
        content: Text('${decode['error']}'),
        actions: [
          FlatButton(
            child: Text('Continuar'),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      )
    );
  }
}