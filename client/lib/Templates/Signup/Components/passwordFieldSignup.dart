import 'package:desculpas_chefe/Templates/Components/ValidateFields.dart';
import 'package:flutter/material.dart';

import 'fieldDecoration.dart';

passwordFieldSignup(_password) {
  return Builder(builder: (context) => Container(
    padding: EdgeInsets.only(bottom: 20),
    child: TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: _password,
      obscureText: true,
      decoration: decoration('Senha', Icons.lock_outline),
      textInputAction: TextInputAction.next,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
      validator: (value) => ValidateFields.validateIfEmpty(value),
    )
  ));
}




