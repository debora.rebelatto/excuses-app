import 'package:desculpas_chefe/Contollers/UserController.dart';
import 'package:flutter/material.dart';

import 'confirmation.dart';

class SignupButton extends StatelessWidget {
  final String _email;
  final String _password;
  final String _name;
  final String _career;
  final _formKey;

  SignupButton(
    this._name,
    this._email,
    this._password,
    this._career,
    this._formKey,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(top: 10),
      child: FlatButton(
        color: Color(0xff2C44C9),
        padding: EdgeInsets.all(15),
        child: Text('Concluír', style: TextStyle(color: Colors.white, fontSize: 18),),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            var res = await UserController.signup(_name, _email, _password, _career);
            await confirmation(res, context, _email, _password);
          }
        }
      )
    );
  }
}
