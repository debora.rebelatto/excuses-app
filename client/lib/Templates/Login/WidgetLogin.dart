import 'package:desculpas_chefe/Templates/Components/facebookButton.dart';
import 'package:desculpas_chefe/Templates/Components/googleButton.dart';
import 'package:desculpas_chefe/Templates/Settings/WidgetSettings.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'Components/LoginForm.dart';
import 'Components/header.dart';
import 'Components/signupButton.dart';

class WidgetLogin extends StatefulWidget{ _WidgetLoginState createState() => _WidgetLoginState(); }

class _WidgetLoginState extends State<WidgetLogin> {

  bool remember = true;
  var erro = '';
  var _scrollController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfffafafa),
      body: Stack(children: [
        NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[ SliverAppBar(
              iconTheme: IconThemeData( color: Colors.black ),
              backgroundColor: Colors.transparent,
              actions: [
                IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetSettings()));
                  },
                )
              ],
            )];
          },
          body: SingleChildScrollView(
            controller: _scrollController,
            child: Center(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    header(),
                    SizedBox(height: 30), LoginForm(), SizedBox(height: 10),
                    Text('OU', style: GoogleFonts.abel(color: Color(0xff2C44C9), fontSize: 20)),
                    googleButton(context),
                    facebookButton(context),
                    signupButton(context)
                  ]
                )
              ),
            ),
          )
        )
      ],)
    );
  }
}
