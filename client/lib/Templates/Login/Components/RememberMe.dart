import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RememberMe extends StatefulWidget {
  _RememberMeState createState() => _RememberMeState();
}

class _RememberMeState extends State<RememberMe> {
  var remember;
  void setRemember() { setState(() { remember = !remember; }); }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      child: Material(
        color: Colors.transparent,
        child: Ink(child: InkWell(
          onTap: () { setRemember(); },
          child: Row(children: [
            Checkbox(
              activeColor: Color(0xff2C44C9),
              value: remember,
              onChanged: (value) { setRemember(); }
            ),
            Text('Lembrar-se de mim', style: GoogleFonts.abel(color: Color(0xff2C44C9), fontSize: 18))
          ])
        )),
      ),
    );
  }
}