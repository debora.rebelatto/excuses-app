import 'package:desculpas_chefe/Contollers/SharedPrefController.dart';
import 'package:desculpas_chefe/Contollers/UserController.dart';
import 'package:desculpas_chefe/Templates/Homepage/WidgetHomepage.dart';
import 'package:flutter/material.dart';

loginButton(String email, String password, var formKey) {
  return Builder(builder: (context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      width: MediaQuery.of(context).size.width * 0.9,
      child: FlatButton(
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
        color: Color(0xff2C44C9),
        child: ListTile(
          title: Text('ENTRAR', textAlign: TextAlign.center, style: TextStyle(color: Colors.white ),),
        ),
        onPressed: () async {
          if (formKey.currentState.validate()) {
            var res = await UserController.login(email: email, password: password);
            if(res.statusCode == 200) {
              await SharedPrefController.saveUser(res);
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetHomePage()));
            }
          }
        },
      )
    );
  });
}