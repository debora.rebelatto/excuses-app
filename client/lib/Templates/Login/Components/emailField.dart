import 'package:desculpas_chefe/Templates/Components/ValidateFields.dart';
import 'package:desculpas_chefe/Templates/Signup/Components/fieldDecoration.dart';
import 'package:flutter/material.dart';

emailField(_email){
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _email,
        decoration: decoration('E-mail', Icons.person),
        validator: (value) => ValidateFields.validateEmail(value),
        textInputAction: TextInputAction.next,
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
      ),
    );
  },);
}