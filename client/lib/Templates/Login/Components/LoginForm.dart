
import 'package:desculpas_chefe/Templates/ForgotPassword/WidgetForgotPassWord.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'PasswordField.dart';
import 'emailField.dart';
import 'loginButton.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm>{
  final _formKey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController(text: "");
  TextEditingController _password = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(children: [
        emailField(_email),
        PasswordField(_password),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            //RememberMe(),

            FlatButton(
              child: Text('Esqueci minha senha',
                style: GoogleFonts.abel(color: Color(0xff2C44C9), fontSize: 18)
              ),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => WidgetForgotPassWord()
                ));
              },
            )
          ],
        ),

        loginButton( _email.text, _password.text, _formKey),
      ],),
    );
  }
}


