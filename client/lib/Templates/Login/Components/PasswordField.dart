import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PasswordField extends StatefulWidget {
  var _password;

  PasswordField(this._password);

  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _obscureText = true;

  _toggle() {
    setState(() { _obscureText = !_obscureText; });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget._password,
      obscureText: _obscureText,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.lock_outline),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(30.0) ),
        ),
        filled: true,
        fillColor: Colors.grey[200],
        hintText: 'Senha',
        suffixIcon: IconButton(
          icon: Icon( _obscureText ? Icons.visibility : Icons.visibility_off ),
          onPressed: _toggle,
        )
      ),
      textInputAction: TextInputAction.next,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
      validator: (value) => value.isEmpty ? 'Preencher campo' : null
    );
  }
}