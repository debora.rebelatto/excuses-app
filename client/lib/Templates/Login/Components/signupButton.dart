import 'package:desculpas_chefe/Templates/Signup/WidgetSignup.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

signupButton(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(top: 10),
    child: FlatButton(
      child: Text(
        'Não possuí conta?',
        style: GoogleFonts.abel(color: Color(0xff2C44C9), fontSize: 20)
      ),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetSignup()));
      },
    )
  );
}
