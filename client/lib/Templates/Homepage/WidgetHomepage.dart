import 'package:desculpas_chefe/Templates/Drawer/WidgetDrawer.dart';
import 'package:desculpas_chefe/Templates/Excuses/WidgetExcuses.dart';
import 'package:desculpas_chefe/Templates/Profile/WidgetProfile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WidgetHomePage extends StatefulWidget{
  _WidgetHomePageState createState() => _WidgetHomePageState();
}

class _WidgetHomePageState extends State<WidgetHomePage> {
  var name;
  List<String> types = ['Falta', 'Atraso', 'Demissão', 'Entrega', 'Outros', 'Outros'];
  List icons = [
    Icons.remove_circle,
    Icons.access_alarm,
    Icons.sentiment_very_dissatisfied,
    Icons.access_alarm,
    Icons.access_alarm,
    Icons.access_alarm
  ];

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() { name = pref.getString('name') ?? ''; });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.transparent,
            actions: [
              FlatButton(
                child: Row(children: [
                  Text('Olá, $name', style: TextStyle(color: Colors.white, fontSize: 20)),
                  Padding(padding: EdgeInsets.only(left: 5), child: Icon(Icons.person_outline, color: Colors.white, size: 30,))
                ]),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetProfile()));
                },
              )
            ],
          ),

          SliverList(
            delegate: SliverChildListDelegate([
              SingleChildScrollView( child: Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0),),
                  child: Material(
                    shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0),),
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(children: [
                          Padding(
                            padding: EdgeInsets.only(top: 20, bottom: 20),
                            child: Text("Categorias", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                          ),
                          GridView.count(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            crossAxisCount: 2,
                            children: List.generate(types.length, (index) => categories(index))
                          ),
                        ],)
                      )
                    )
                  )
                )
              ))
            ]),
          ),
        ],
      ),
      drawer: WidgetDrawer(),
    );
  }

  categories(var index){
    return Card(
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0),),
      child: Material(
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0),),
        color: Color(0xff2C44C9),
        child: Ink(child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetExcuses(types[index])));
          },
          child: Center(child: FittedBox(child: Column(children: [
            Icon(icons[index], color: Colors.white, size: 30),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.only(bottom: 0),
              child: Text(types[index], style: TextStyle(color: Colors.white, fontSize: 25))
            ),
          ])))
        )),
      )
    );
  }
}
