import 'package:desculpas_chefe/Templates/Info/Info.dart';
import 'package:desculpas_chefe/Templates/Profile/WidgetProfile.dart';
import 'package:flutter/material.dart';
import 'DrawerItem.dart';
import 'UserDetail.dart';

class WidgetDrawer extends StatefulWidget {
  _WidgetDrawerState createState() => _WidgetDrawerState();
}

class _WidgetDrawerState extends State<WidgetDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(children: [
        Expanded( child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            UserDetail(),
            DrawerItem(Icon(Icons.person), 'Perfil', page: WidgetProfile()),
            DrawerItem(Icon(Icons.exit_to_app), 'Logout'),
          ],
        )),
        Container(padding: EdgeInsets.only(left: 10, right: 10), child: Divider(color: Colors.grey)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
          Container(
            alignment: Alignment.bottomLeft,
            child: FlatButton(
              child: Icon(Icons.info_outline),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => Info()));
              },
            )
          ),
          //Container(alignment: Alignment.bottomRight, child: SwitchTheme())
        ])
      ],)
    );
  }
}