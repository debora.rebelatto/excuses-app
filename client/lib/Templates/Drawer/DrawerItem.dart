import 'package:desculpas_chefe/Contollers/SharedPrefController.dart';
import 'package:desculpas_chefe/Templates/Login/WidgetLogin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerItem extends StatefulWidget{
  final Icon icon;
  final String name;
  final Widget page;
  DrawerItem(this.icon, this.name, { this.page });
  _DrawerItemState createState() => _DrawerItemState();
}

class _DrawerItemState extends State<DrawerItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: widget.icon, title: Text(widget.name),
      onTap: () {
        if(widget.page != null) {
          Navigator.pop(context);
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => widget.page));
        } else {
          SharedPrefController.logoutUser();
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => widget.page));
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => WidgetLogin()),
            ModalRoute.withName('/'),
          );
        }
      },
    );
  }
}