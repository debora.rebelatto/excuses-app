import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

class SwitchTheme extends StatelessWidget {
@override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.brightness_6),
      onPressed: () {
        DynamicTheme.of(context).setBrightness(
          Theme.of(context).brightness == Brightness.dark ? Brightness.light : Brightness.dark
        );
      },
    );
  }
}