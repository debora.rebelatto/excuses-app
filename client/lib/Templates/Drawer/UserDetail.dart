import 'package:desculpas_chefe/Templates/Profile/WidgetProfile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserDetail extends StatefulWidget { UserDetailState createState() => UserDetailState(); }

class UserDetailState extends State<UserDetail>{
  var name;
  var email;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      name = pref.getString('name') ?? '';
      email = pref.getString('email') ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 90,
        child: UserAccountsDrawerHeader(
          decoration: BoxDecoration(color: Theme.of(context).bottomAppBarColor),
          accountName: Container(
            padding: EdgeInsets.only(top: 30),
            child: Text(name,
              style: Theme.of(context).primaryTextTheme.subtitle1
            )
          ),
          accountEmail: Container(
            child: Text(email,
              style: Theme.of(context).primaryTextTheme.subtitle1
            )
          )
        )
      ),

      onTap: () {
        Navigator.pop(context);
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetProfile()));
      },
    );
  }
}