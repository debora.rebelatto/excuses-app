import 'package:desculpas_chefe/Templates/Signup/WidgetSignup.dart';
import 'package:flutter/material.dart';

signupButton() {
  return Builder(builder: (context) {
    return
  Container(
    padding: EdgeInsets.only(top: 20),
    width: MediaQuery.of(context).size.width * 0.7,
    child: OutlinedButton(
      style: OutlinedButton.styleFrom(
        backgroundColor: Colors.white,
        padding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40),
        ),
        side: BorderSide(width: 1, color: Colors.grey),
      ),
      child: Text('CADASTRE-SE', style: TextStyle(color: Colors.black, fontSize: 18),),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetSignup()));
      },
    )
  );
  },);


}


