import 'package:flutter/material.dart';
import 'package:desculpas_chefe/Templates/Login/WidgetLogin.dart';

loginButton() {
  return Builder(
    builder: (context) {
      return FlatButton(
        child: Text('Já possuí conta? Faça login', style: TextStyle(color: Colors.white, fontSize: 15)),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => WidgetLogin()));
        }
      );
    }
  );
}

