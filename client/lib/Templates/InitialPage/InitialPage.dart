import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'Components/SignupButton.dart';
import 'Components/loginButton.dart';


class InitialPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 30),
              Text('Vira Lata Caramelo',
                style: TextStyle(fontFamily: 'SergioTrendy', fontSize: 20, color: Colors.white),
              ),
              SizedBox(height: 50),
              Image.asset('assets/images/016.png', height: 300),
              SizedBox(height: 30),
              Text('Aproveite mais a vida',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  textStyle: TextStyle(color: Colors.white, fontSize: 30),
                )
              ),
              SizedBox(height: 15),

              Text('Se estresse menos',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  textStyle: TextStyle(color: Colors.white, fontSize: 30),
                )
              ),

              SizedBox(height: 10),
              signupButton(),
              SizedBox(height: 10),
              loginButton(),
              SizedBox(height: 30),
            ],
          )
        ),
      )
    );
  }
}