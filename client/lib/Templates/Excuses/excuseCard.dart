import 'package:flutter/material.dart';

excuseCard(excuse) {
  return Builder(
    builder: (context) {
      return Container(
        padding: EdgeInsets.all(20),
        height: 200,
        width: MediaQuery.of(context).size.width,
        child: Card(
          color: Color(0xff2C44C9),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(child: Text(excuse,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                )
              ))
            ]
          )
        )
      );
    },
  );
}