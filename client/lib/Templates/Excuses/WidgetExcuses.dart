import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

import 'Components/header.dart';
import 'excuseCard.dart';

class WidgetExcuses extends StatefulWidget {
  final String title;
  WidgetExcuses(this.title);
  _WidgetExcusesState createState() => _WidgetExcusesState();
}

class _WidgetExcusesState extends State<WidgetExcuses> {
  var excuse = 'Clique no botão para gerar desculpa';

  generateExcuse() {
    setState(() {
      excuse = WordPair.random().toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[ SliverAppBar( title: Text('Gerador de Desculpas'), backgroundColor: Colors.transparent )];
            },
            body: Padding(
              padding: EdgeInsets.all(0),
              child: SingleChildScrollView(
                child: Stack(children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          child: SingleChildScrollView(
                            child: Column(children: [
                              header(widget.title),
                              excuseCard(excuse),
                              buttonGenerate()
                            ],)
                          )
                        ),
                      ),
                    ),
                  ),
                ],)
              )
            )
          ),
        ],
      ),
    );
  }

  buttonGenerate() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: MaterialButton(
          color: Color(0xff2C44C9),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40)
          ),
          onPressed: () { generateExcuse(); },
          child: Text('Gerar Desculpa', style: TextStyle(color: Colors.white))
        )
      )
    );
  }
}
