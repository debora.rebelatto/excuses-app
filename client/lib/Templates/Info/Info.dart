import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(title: Text('Info'), backgroundColor: Color(0xff2C44C9),),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Vira Lata Caramelo Inc.', style: TextStyle(fontSize: 20),),
            Text('Desculpas Chefe', style: TextStyle(fontSize: 20),)
          ],
        ),
      )
    );
  }
}