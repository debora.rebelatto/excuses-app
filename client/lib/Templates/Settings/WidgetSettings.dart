import 'package:desculpas_chefe/Contollers/ApiUrl.dart';
import 'package:desculpas_chefe/Contollers/PortController.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WidgetSettings extends StatefulWidget {
  WidgetSettings({ Key key }) : super(key: key);

  @override
  _WidgetSettingsState createState() => _WidgetSettingsState();
}

class _WidgetSettingsState extends State<WidgetSettings> {
  TextEditingController host = TextEditingController(text: '');

  dynamic _getUrl() async {
    var pref = await SharedPreferences.getInstance();

    setState(() {
      host.text = pref.getString('url') ?? ApiUrl.url;
    });
  }

  @override
  void initState() {
    super.initState();
    _getUrl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(title: Text('Configurações'), backgroundColor: Colors.blue),
      body: Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Container( child: TextField( controller: host, maxLines: 1, decoration: InputDecoration( labelText: 'IP/Host') )),
            SizedBox( height: 10 ),
            Container(
              padding: EdgeInsets.only( top: 10 ),
              child: FlatButton(
                color: Colors.blue,
                child: Text('Salvar', style: TextStyle( fontWeight: FontWeight.bold, color: Colors.white )),
                onPressed: () { updateHostPort(); },
              )
            )
          ],
        )
      ),
    );
  }

  dynamic updateHostPort() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Salvar as alterações?'),
          content: Text('Host: ${host.text}'),
          actions: [ accept(), cancel() ],
        );
      },
    );
  }

  dynamic accept() {
    return FlatButton(
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0) ),
      child: Text( 'Sim', style: TextStyle( color: Colors.white), ),
      color: Colors.blueAccent,
      onPressed: () {
        update(host.text);
        Navigator.of(context).pop();
      },
    );
  }

  dynamic update(String newHost) async {
    await PortController.updateHostPort(url: newHost);
    setState(() {
      host.text = newHost;
    });
  }

  dynamic cancel() => Builder(builder: (context) => FlatButton(
    shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0) ),
    child: Text('Cancelar', style: TextStyle( color: Colors.white) ),
    color: Colors.red[700],
    onPressed: () async { Navigator.of(context).pop(); },
  ));
}
