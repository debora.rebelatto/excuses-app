import 'package:flutter/material.dart';

profilePicture() {
  return Align(
    alignment: Alignment.topCenter,
    child: ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Image.network(
        'https://images.unsplash.com/photo-1605468546295-a3730c3eeed2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
        fit: BoxFit.cover, height: 150.0, width: 150.0,
      ),
    )
  );
}