import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInfo extends StatefulWidget{
  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  var name;
  var email;
  var career;
  var appPlan;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      name = pref.getString('name') ?? '';
      email = pref.getString('email') ?? '';
      career = pref.getString('career') ?? '';
      appPlan = pref.getString('appPlan') ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 130, 10, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ProfileInfo('Nome: ', name),
          ProfileInfo('Email: ', email),
          ProfileInfo('Plano: ', appPlan),
          ProfileInfo('Profissão: ', career),
        ]
      )
    );
  }
}

// ignore: must_be_immutable
class ProfileInfo extends StatelessWidget {
  final String text;
  var info;

  ProfileInfo(this.text, this.info);

  @override
  Widget build(BuildContext context) {
    if(info == null) info = '';
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: RichText(
        text: TextSpan(
          text: text, style: TextStyle(
            color: Theme.of(context).primaryTextTheme.subtitle1.color,
            fontWeight: FontWeight.bold,
            fontSize: 17
          ),
          children: [
            TextSpan(
              text: info,
              style: TextStyle(
                color: Theme.of(context).primaryTextTheme.subtitle1.color,
                fontWeight: FontWeight.normal
              )
            )
          ],
        ),
      )
    );
  }
}