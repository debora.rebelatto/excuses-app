import 'package:desculpas_chefe/Templates/Settings/WidgetSettings.dart';
import 'package:flutter/material.dart';
import 'Components/UserInfo.dart';
import 'Components/profilePicture.dart';

class WidgetProfile extends StatefulWidget { _WidgetProfileState createState() => _WidgetProfileState(); }

class _WidgetProfileState extends State<WidgetProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[ SliverAppBar( title: Text('Perfil'), backgroundColor: Colors.transparent )];
            },
            body: Padding(
              padding: EdgeInsets.all(0),
              child: SingleChildScrollView(
                child: Stack(children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: 300,
                        width: MediaQuery.of(context).size.width,
                        child: Card(child: Stack(children: [ settingsButton(), UserInfo() ])),
                      ),
                    ),
                  ),
                  profilePicture()
                ],)
              )
            )
          ),
        ],
      ),
    );
  }

  settingsButton() {
    return Align(
      alignment: Alignment.topRight,
      child: Container(
        child: IconButton( icon: Icon(Icons.settings),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => WidgetSettings()
            ));
          }
        )
      )
    );
  }
}

