import 'package:desculpas_chefe/theme.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'Templates/InitialPage/InitialPage.dart';

void main() { runApp(MyApp()); }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => theme(brightness),
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          theme: theme,
          debugShowCheckedModeBanner: false,
          title: 'Desculpas Chefe',
          home: InitialPage()
        );
      }
    );
  }
}
