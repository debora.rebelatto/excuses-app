import 'dart:convert';

import 'package:desculpas_chefe/Contollers/UserController.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedPrefController {
  static saveUser(res) async {
    var pref = await SharedPreferences.getInstance();
    var decodeLogin = jsonDecode(res.body);

    await pref.setString('token', decodeLogin['token']);
    await pref.setString('userId', decodeLogin['user']['_id']);

    var user = await UserController.getById(decodeLogin['user']['_id']);

    var decodeUser = jsonDecode(user.body);

    await pref.setString('name', decodeUser['name']);
    await pref.setString('email', decodeUser['email']);
    await pref.setString('appPlan', 'Mastermind');
    await pref.setString('career', decodeUser['profession']);
  }

  static logoutUser() async {
    var pref = await SharedPreferences.getInstance();

    await pref.remove('token',);
    await pref.remove('userId');
    await pref.remove('name');
    await pref.remove('email');
    await pref.remove('appPlan');
    await pref.remove('career');
  }
}