import 'package:shared_preferences/shared_preferences.dart';

import 'ApiUrl.dart';

class PortController {
  static dynamic updateHostPort({ String url }) async {
    var pref = await SharedPreferences.getInstance();
    if(url != null) await pref.setString('url', url);
  }

  static Future<String> getHostPort() async {
    var pref = await SharedPreferences.getInstance();
    var url = pref.getString('url') ?? ApiUrl.url;
    return url;
  }
}
