import 'dart:convert';
import 'package:desculpas_chefe/Contollers/API.dart';

abstract class UserController {
  static login({ String email, String password }) async {
    try{
      var res = await HTTP.httpPost('/auth/authenticate',
        body: jsonEncode({ "email": email, "password": password })
      );
      return res;
    } catch(err) {
      print(err);
    }
  }

  static signup(String name, String email, String password, String career) async {
    try {
      return await HTTP.httpPost('/auth/register',
        body: jsonEncode({
          "name": name,
          "email": email,
          "password": password,
          "profession": career
        })
      );
    } catch (err) {
      print(err);
    }
  }

  static getById(id) async {
    try {
      return await HTTP.httpGet('/user/$id');
    } catch (err) {
      print(err);
    }
  }
}