import 'dart:io';

import 'package:http/http.dart' as http;

import 'ApiUrl.dart';
import 'PortController.dart';


abstract class HTTP {
  static Future buildHost() async {
    try {
      return await PortController.getHostPort();
    } catch(event) {
      return ApiUrl.url;
    }
  }

  static buildHeader() async => {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.acceptHeader: 'application/json',
  };

  static httpPost(String url, { dynamic body }) async {
    try{
      return await http.post(await buildHost() + url, headers: await buildHeader(), body: body);
    } catch(err) {
      print(err);
    }
  }

  static httpGet(String url) async {
    return await http.get(await buildHost() + url, headers: await buildHeader());
  }

  static httpPut(String url, { dynamic body }) async {
    return await http.put(await buildHost() + url, headers: await buildHeader());
  }

  static httpDelete(String url) async {
    return await http.put(await buildHost() + url, headers: await buildHeader());
  }
}