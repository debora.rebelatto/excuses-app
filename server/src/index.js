const express = require("express");
var cors = require('cors');

const app = express();

require('dotenv').config()

app.use(express.json());

require('./app/controllers/authController')(app);
require('./app/controllers/userController')(app);
require('./app/controllers/projectController')(app);

app.listen(3000, () => console.log("running on port 3000"))