const express = require("express");

const User = require('../models/User');

const router = express.Router();

router.get('/:id', async (req, res) => {
  var id = req.params.id;
  var user = await User.findOne({ _id: id })
  return res.status(200).send( user );
});

module.exports = app => app.use("/user", router);