const mongoose = require('../database/database')

const ExcuseSchema = mongoose.Schema({
  text: {
    type: String
  },
  sentTime: {
    type: Date
  },
  score: {
    type: Number
  }
})

const Excuse = mongoose.model('Excuse', ExcuseSchema);

module.exports = Excuse;